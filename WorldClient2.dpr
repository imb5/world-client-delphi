program WorldClient2;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  MyConsole,
  Logger,
  LogFile,
  LogConsole,
  StdIni,
  imb5,
  imb.SocksLib,
  WorldDataCode,
  WorldDataImage,
  WorldClientLib,
  System.Generics.Collections,
  System.SysUtils;

const
  BinName = 'us_ams_2017.V7';

function wiretypeAsStr(aWireType: Integer): string;
begin
  case aWireType of
    wtVarInt: Result := 'varInt';
    wt32Bit: Result := '32Bit';
    wt64Bit: Result := '64Bit';
    wtLengthDelimited: Result := 'len';
  else
    Result := '##'+aWireType.ToString();
  end;
end;

procedure ConsoleLn(const aLine: string);
begin
  TMonitor.Enter(log);
  try
    WriteLn(aLine);
  finally
    TMonitor.Exit(log);
  end;
end;

//type
//  tagListHander = reference to procedure(const aID: TWDID; aSender: TObject; aKey: TWDKey; const aValue: TWDValue);
{
procedure handleGeneRoadEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
var
  tagList: TDictionary<TWDKey, tagListHander>;
begin
  // todo: implement object handling
  tagList := TDictionary<TWDKey, tagListHander>.Create;
  try
    tagList.Add(wdat,
      procedure(const aID: TWDID; aSender: TObject; aKey: TWDKey; const aValue: TWDValue)
      begin
        (aSender as TMyObject)
      end);
  finally
    tagList.Free;
  end;
end;
}

type
  TRoad = class
  constructor Create;
  public
    id: TWDID;
    // attributes
    SNELHEID: Double;
    STRAATNAAM: string;
  end;

{ TRoad }

constructor TRoad.Create;
begin
  inherited Create;
  ID := '';
  SNELHEID := Double.NaN;
  STRAATNAAM := '';
end;

procedure HandleIMBException(aConncetion: TConnection; aException: Exception);
begin
  Log.WriteLn('IMB reader exception: '+aException.Message, llError);
end;

procedure HandleIMBDisconnect(aConncetion: TConnection);
begin
  Log.WriteLn('IMB disconnect ('+WSAGetLastError.ToString+')', llWarning);
end;

procedure Start;
var
  remoteHost: string;
  remotePort: Integer;
  prefix: string;
  connection: TConnection;
  dataSources: TWDDataSources;
  bin: TWDBin;
  gene_road: TWDCollection<TRoad>;
  r: TRoad;
  firstID: TWDID;
  updater: TWDCObjectUpdate;
  nscp: TPair<string, TWDSchemaCollection>;
//  nsap: TPair<TWDKey, TWDSchemaAttribute>;
  nsap: TPair<string, TWDSchemaAttribute>;
  //collectionEvent: TEventEntry;
  cnt: Integer;
begin
  cnt := 0;
  remoteHost := GetSetting(RemoteHostSwitch, imbDefaultRemoteHost);//'app-usdebug01.tsn.tno.nl');//imbDefaultRemoteHost);
  remotePort := GetSetting(RemotePortSwitch, imbDefaultRemoteSocketPort);
  prefix := GetSetting(prefixSwitch, DefaultPrefix);
  connection := TSocketConnection.Create('WorldClient', 1, prefix, remoteHost, remotePort, True);
  try
    connection.onException := HandleIMBException;
    connection.onDisconnect := HandleIMBDisconnect;
    dataSources := TWDDataSources.Create(connection);
    try

      // connect to given bin
      bin := dataSources.Bin(BinName);
      if Assigned(bin) then
      begin
        ConsoleLn('Waiting for bin load');
        if bin.Load then
        begin
          ConsoleLn('bin loaded');

          gene_road := bin.Collection<TRoad>('GENE_ROAD');

          gene_road.RegisterAttributeClear;
          gene_road.RegisterAttribute(538 shr 3,
            procedure(aCollection: TWDCollection<TRoad>; const aID: TWDID; var aObject: TRoad; aTag: Integer; const aValue: string)
            begin
              aObject.STRAATNAAM := aValue;
              //Sleep(1000); // todo: remove after stress tests
            end);
          gene_road.RegisterAttribute(521 shr 3,
            procedure(aCollection: TWDCollection<TRoad>; const aID: TWDID; var aObject: TRoad; aTag: Integer; aValue: Double)
            begin
              aObject.SNELHEID := aValue;
            end);

          gene_road.OnObject :=
            procedure(aCollection: TWDCollection<TRoad>; const aID: TWDID; out aObject: TRoad)
            begin
              // save first id to use as demo in change object below
              if firstID=''
              then firstID := aID;
              // create an object of <T>
              aObject := TRoad.Create;
              aObject.id := aID;
              cnt := cnt+1;
              Log.Progress(cnt.toString);
            end;

          gene_road.OnNoObject :=
            procedure(aCollection: TWDCollection<TRoad>; const aID: TWDID)
            begin
              // actual remove is done in calling function
            end;
          gene_road.onTruncate :=
            procedure(aCollection: TWDCollection<TRoad>)
            begin
              // actual truncate is done in calling function
            end;

          // start loading data
          gene_road.Inquire('');


          ConsoleLn('Press return to show current schema');
          ReadLn;

          for nscp in dataSources.schema do
          begin
            ConsoleLn(nscp.key);
            for nsap in nscp.Value.attributesOnName do
            begin
              ConsoleLn('   '+nsap.Key+' ('+wiretypeAsStr(nsap.value.Key and 7)+'): '+nsap.Value.key.ToString+', '+nsap.Value.worldFunctions.ToString+', from: '+nsap.Value.source);
            end;
          end;


          // test sending update over imb
          if firstID<>'' then
          begin
            ConsoleLn('Press return to change road');
            ReadLn;

            updater := TWDCObjectUpdate.Create(gene_road.event);
            try
              updater.setID(firstID);
              updater.setValue(538 shr 3, 'de straatnaam');
              updater.Commit;
            finally
              updater.Free;
            end;
          end;

          ConsoleLn('Press return to stop processing..');
          ReadLn;

          // dump road ntries
          for r in gene_road.objects.Values.ToArray do
          begin
            if Assigned(r)
            then ConsoleLn((r as TRoad).SNELHEID.ToString+','+(r as TRoad).STRAATNAAM);
          end;
          ReadLn;
        end
        else
        begin
          ConsoleLn('## Could not load bin');
          ConsoleLn('Press return to exit..');
          ReadLn;
        end;
      end
      else
      begin
        ConsoleLn('>> bin '+ BinName+' not found on source..');
        ConsoleLn('Press return to exit..');
        ReadLn;
      end;

    finally
      connection.Stop;
      dataSources.Free;
    end;
  finally
    connection.Free;
  end;
end;

begin
  System.ReportMemoryLeaksOnShutdown := True;
  try
    Start;
    //ReadLn;
  except
    on E: Exception do
      Log.Writeln(E);
  end;
end.
